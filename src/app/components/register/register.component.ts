import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

interface Register {
  login: string;
  email: string;
  password: string;
  repassword: string;
}

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

registrationForm: FormGroup;

//  constructor() { }

  ngOnInit() {
    this.registrationForm = new FormGroup({
      login: new FormControl('', [Validators.required, Validators.minLength(4)]),
      email: new FormControl('', [Validators.required, Validators.minLength(4)]),
      password: new FormControl('', [Validators.required, Validators.minLength(6)]),
      repassword: new FormControl('', Validators.required)
    });
  }
  onSubmit(form) {
        console.log(form.valid);
        console.log(form.value);
  }
}
