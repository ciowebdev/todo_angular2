import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {
  context = 'Dear friend, if you can read this, you already catch bluebird. Just keep going';
  constructor() { }

  ngOnInit() {
  }

}
