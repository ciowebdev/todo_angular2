import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthUserService } from '../../services/auth-user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private authUserService: AuthUserService, public router: Router) { }

  ngOnInit() {
  }

  routeLogin() {
    this.router.navigate(['/login']);
  }

  logout() {
    this.authUserService.logout();
    this.router.navigate(['']);
  }

}
