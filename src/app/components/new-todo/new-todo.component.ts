import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { TodosDataService } from '../../services/todo-data.service';

@Component({
  selector: 'app-new-todo',
  templateUrl: './new-todo.component.html',
  styleUrls: ['./new-todo.component.css']
})
export class NewTodoComponent {
  title = '';
  myDate = new Date();
  constructor(private todoService: TodosDataService) {}
  addTodo() {
    this.todoService.addEvent.emit( { title: this.title, plannedDate: this.myDate} );
    this.title = '';
    console.log(this.myDate);
  }
 /* constructor() { }

  ngOnInit() {
  }*/

}
