import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { TodosDataService } from '../../services/todo-data.service';

@Component({
  selector: 'app-todo-filters',
  templateUrl: './todo-filters.component.html',
  styleUrls: ['./todo-filters.component.css']
})
export class TodoFiltersComponent implements OnInit {
  filters = {
    'completed': 0,
    'today': false
  };
  constructor(private todoService: TodosDataService) {}

  filterByComplete(filter) {
    this.filters.completed = filter;
    this.todoService.filterEvent.emit(this.filters);
  }

  filterByDate(filter) {
    this.filters.today = !this.filters.today;
    this.todoService.filterEvent.emit(this.filters);
  }

  ngOnInit() {
  }
}
