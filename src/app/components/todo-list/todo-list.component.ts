import { Component, OnInit, Input } from '@angular/core';
import { Todo } from '../../shared/todo';
import { todos } from '../../shared/const';
import { TodosDataService } from '../../services/todo-data.service';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent {

  @Input() todos: Todo[];
  constructor(private todoService: TodosDataService) {}
  toogle(todo: Todo) {
    todo.completed = !todo.completed;
  }

  delete(todo: Todo) {
      this.todoService.deleteEvent.emit(todo.title);
  }

 /* constructor() { }

  ngOnInit() {
  }*/

}
