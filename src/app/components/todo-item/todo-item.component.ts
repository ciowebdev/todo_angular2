import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Todo } from '../../shared/todo';

@Component({
    selector: 'app-todo-item',
    templateUrl: './todo-item.component.html',
    styleUrls: ['./todo-item.component.css']
})
export class TodoItemComponent implements OnInit {
  @Input() todo: Todo;
  @Output() delete = new EventEmitter();
  dateString: string = '';

  ngOnInit() {
     if(this.todo.plannedDate !== undefined) {
       let date = new Date(this.todo.plannedDate);
       this.dateString = date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear(); //date.toLocaleString()
     }
  }

  toggle() {
    this.todo.completed = !this.todo.completed;
  }

  onDelete() {
    this.delete.emit(this.todo);
  }
  // constructor() { }
}
