import { Component, OnInit} from '@angular/core';
import { UserDataService } from '../../services/user-data.service';
import { User } from '../../shared/user-data.model';


@Component({
  selector: 'app-about-page',
  templateUrl: './about-page.component.html',
  styleUrls: ['./about-page.component.css']

})
export class AboutPageComponent implements OnInit {

  users: User[] = [];

  constructor(private userDataService: UserDataService) { }

  ngOnInit() {
    this.userDataService.getUsers()
    .subscribe(userData => this.users = userData
    );

  }

}
