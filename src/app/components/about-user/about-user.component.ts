import { Component, OnInit, Input } from '@angular/core';
import { User } from '../../shared/user-data.model';

@Component({
  selector: 'app-about-user',
  templateUrl: './about-user.component.html',
  styleUrls: ['./about-user.component.css']
})
export class AboutUserComponent implements OnInit {
  @Input() user: User;

  constructor() {
  }

  ngOnInit() {
  }
}
