import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { TodosDataService } from '../../services/todo-data.service';

@Component({
  selector: 'app-todo-search',
  templateUrl: './todo-search.component.html',
  styleUrls: ['./todo-search.component.css']
})
export class TodoSearchComponent implements OnInit {
  searchString = '';
  constructor(private todoService: TodosDataService) {}
  // @Output() search = new EventEmitter();

  searchTodo() {
    this.todoService.searchEvent.emit(this.searchString);
  }

  ngOnInit() {
  }

}
