import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-todo-data',
  templateUrl: './todo-data.component.html',
  styleUrls: ['./todo-data.component.css']
})
export class TodoDataComponent implements OnInit {
  @Input() id: any;

  constructor() { }

  ngOnInit() {
  }

}
