import { Component, Input, OnInit } from '@angular/core';

import { Todo } from '../../shared/todo';
import { TodosDataService } from '../../services/todo-data.service';
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'app-todo-page',
  templateUrl: './todo-page.component.html',
  styleUrls: ['./todo-page.component.css']
})
export class TodoPageComponent  implements OnInit {
  @Input() todos: Observable<Todo[]>;

  constructor(private todosService: TodosDataService) {
        this.todosService.searchEvent
            .subscribe(
              params => this.todos = this.todosService.getTodosSearch(params),
              error => console.error(error),
              () => console.log('DONE')
            );
        this.todosService.addEvent
            .subscribe(
              params => this.todos = this.todosService.addTodo(params),
              error => console.error(error),
              () => console.log('DONE')
            );
        this.todosService.deleteEvent
            .subscribe(
              params => this.todos = this.todosService.deleteTodo(params),
              error => console.error(error),
              () => console.log('DONE')
            );
         this.todosService.filterEvent
            .subscribe(
              params => this.todos = this.todosService.getTodosFilter(params),
              error => console.error(error),
              () => console.log('DONE')
            );
  }

  ngOnInit() {
    this.todos = this.todosService.getTodos();
  }


  }
