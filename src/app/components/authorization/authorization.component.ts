import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthUserService } from '../../services/auth-user.service';

interface Login {
  login: string;
  password: string;
}

@Component({
  selector: 'app-authorization',
  templateUrl: './authorization.component.html',
  styleUrls: ['./authorization.component.css']
})
export class AuthorizationComponent implements OnInit {

authorizationForm: FormGroup;
private isFailedLogin = false;

constructor(private authUserService: AuthUserService, public router: Router) { }

  ngOnInit() {
    this.authorizationForm = new FormGroup({
      login: new FormControl('', [Validators.required, Validators.minLength(4)]),
      password: new FormControl('', [Validators.required, Validators.minLength(6)])
    });
    this.isFailedLogin = false;
  }
  onSubmit(form) {
        console.log(form.valid);
        console.log(form.value);
        this.authUserService.login(form.value.login, form.value.password).subscribe(() => {
            if (this.authUserService.isLoggedIn) {
                const redirect = this.authUserService.redirectUrl ? this.authUserService.redirectUrl : '';
                this.router.navigate([redirect]);
            } else {
                this.isFailedLogin = true;
            }
        });
  }
}
