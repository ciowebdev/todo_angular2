import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule, Http } from '@angular/http';
import { RouterModule } from '@angular/router';
import { NguiDatetimePickerModule } from '@ngui/datetime-picker';
import { routing } from './app.routing';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { NewTodoComponent } from './components/new-todo/new-todo.component';
import { TodoListComponent } from './components/todo-list/todo-list.component';
import { TodoItemComponent } from './components/todo-item/todo-item.component';
import { TodoSearchComponent } from './components/todo-search/todo-search.component';
import { TodoFiltersComponent } from './components/todo-filters/todo-filters.component';
import { TodoPageComponent } from './components/todo-page/todo-page.component';
import { AboutPageComponent } from './components/about-page/about-page.component';
import { AboutUserComponent } from './components/about-user/about-user.component';
import { HomePageComponent } from './components/home-page/home-page.component';
import { AuthorizationComponent } from './components/authorization/authorization.component';
import { RegisterComponent } from './components/register/register.component';

import { UserDataService } from './services/user-data.service';
import { TodosDataService } from './services/todo-data.service';
import { AuthUserService } from './services/auth-user.service';
import { AuthGuard } from './services/auth-guard.service';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TodoDataComponent } from './components/todo-data/todo-data.component';

export function HttpLoaderFactory(http: Http) {
    return new TranslateHttpLoader(http, '../assets/translate/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    NewTodoComponent,
    TodoListComponent,
    TodoItemComponent,
    TodoSearchComponent,
    TodoFiltersComponent,
    TodoPageComponent,
    AboutPageComponent,
    AboutUserComponent,
    HomePageComponent,
    AuthorizationComponent,
    RegisterComponent,
    TodoDataComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    RouterModule,
    NguiDatetimePickerModule,
    routing,
    TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [Http]
            }
        })
  ],
  providers: [UserDataService, TodosDataService, AuthUserService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
