export class Todo {
      constructor( public title: string,
                   public completed: boolean = false,
                   public plannedDate: Date) {}
};

