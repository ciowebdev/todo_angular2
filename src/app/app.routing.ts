import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { TodoPageComponent } from './components/todo-page/todo-page.component';
import { AboutPageComponent } from './components/about-page/about-page.component';
import { HomePageComponent } from './components/home-page/home-page.component';
import { AuthorizationComponent } from './components/authorization/authorization.component';
import { RegisterComponent } from './components/register/register.component';

import { AuthGuard } from './services/auth-guard.service';

const routes: Routes = [
  {path: '', component: HomePageComponent},
  {path: 'todos', component: TodoPageComponent, canActivate: [AuthGuard]},
  {path: 'about', component: AboutPageComponent},
  {path: 'login', component: AuthorizationComponent},
  {path: 'signin', component: RegisterComponent}
];

export const routing = RouterModule.forRoot(routes);
