import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthUserService } from './auth-user.service';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private authUserService: AuthUserService, private router: Router) { }
    // ActivatedRouteSnapshot - информация о маршруте связанного с загруженым компонентом.
    // RouterStateSnapshot - состояние маршрута в определенный отрезок времени.
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

        if (this.authUserService.isLoggedIn) {
            return true;
        } else {
            this.authUserService.redirectUrl = state.url;
            this.router.navigate(['/login']);
            return false;
        }
    }
}
