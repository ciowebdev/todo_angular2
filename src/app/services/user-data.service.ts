
import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Response} from '@angular/http';
import {User} from '../shared/user-data.model';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class UserDataService {

   // private userdataUrl = '../assets/user-data.json';

    constructor(private http: Http) {}

    getUsers(): Observable<User[]> {
        //return this.http.get(this.userdataUrl)
         return this.http.get('/about_users/get')
                        .map((res: Response) => <User[]>res.json().data);
    }
}
