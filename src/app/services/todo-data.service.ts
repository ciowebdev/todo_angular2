import { Injectable, EventEmitter } from '@angular/core';
import { Http } from '@angular/http';
import { Response, Headers, RequestOptions } from '@angular/http';
import { Todo } from '../shared/todo';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class TodosDataService {
    searchEvent: EventEmitter<any> = new EventEmitter();
    addEvent: EventEmitter<any> = new EventEmitter();
    deleteEvent: EventEmitter<any> = new EventEmitter();
    filterEvent: EventEmitter<any> = new EventEmitter();
    constructor(private http: Http) {}
    getTodos(): Observable<Todo[]> {
        return this.http.get('/api/get-todos')
                        .map((resp: Response) => {
                            let todoList = resp.json().data;
                            let todos: Todo[] = [];
                             for ( let index in todoList ) {
                                  if (todoList.hasOwnProperty(index)) {
                                    let todo = todoList[index];
                                    todos.push({title: todo.title, completed: todo.completed, plannedDate: todo.plannedDate});
                                  }
                            }
                            return todos;
                        });
    }
    getTodosSearch(params): Observable<Todo[]> {
        return this.http.get('/api/get-todos')
                        .map((resp: Response) => {
                            let todoList = resp.json().data;
                            let todos: Todo[] = [];
                             for ( let index in todoList ) {
                                  if (todoList.hasOwnProperty(index) && todoList[index].title.includes(params)) {
                                    let todo = todoList[index];
                                    todos.push({title: todo.title, completed: todo.completed, plannedDate: todo.plannedDate });
                                  }
                            }
                            return todos;
                        });
    }
    getTodosFilter(params): Observable<Todo[]> {
        return this.http.get('/api/get-todos')
            .map((resp: Response) => {
                let todoList = resp.json().data;
                let todos: Todo[] = [];
                let now_date = new Date();
                    for ( let index in todoList ) {
                        let todo = todoList[index];
                        let date = new Date(todo.plannedDate);

                        if (params.completed == 0) {
                            if(params.today) {
                                if(date.getDate() == now_date.getDate() && date.getMonth() == now_date.getMonth() && date.getFullYear() == now_date.getFullYear())
                                    todos.push({title: todo.title, completed: todo.completed, plannedDate: todo.plannedDate });
                            }
                            else
                                todos.push({title: todo.title, completed: todo.completed, plannedDate: todo.plannedDate });
                        }
                        else {
                            let complete = (params.completed == 1) ? false : true;
                            if (todo.completed == complete) {
                                if(params.today) {
                                    if(date.getDate() == now_date.getDate() && date.getMonth() == now_date.getMonth() && date.getFullYear() == now_date.getFullYear())
                                        todos.push({title: todo.title, completed: todo.completed, plannedDate: todo.plannedDate });
                                }
                                else
                                    todos.push({title: todo.title, completed: todo.completed, plannedDate: todo.plannedDate });
                            }
                        }
                }
                return todos;
            });
    }
    addTodo(params): Observable<any> {
        const headers = new Headers({ 'Content-Type': 'application/json;charset=utf-8' });
        const options = new RequestOptions({ headers: headers });
        return this.http.post('/api/add-todo', { params: params }, options)
                             .map((resp: Response) => resp.json().data);
    }
    deleteTodo(params): Observable<any> {
        const headers = new Headers({ 'Content-Type': 'application/json;charset=utf-8' });
        const options = new RequestOptions({ headers: headers });
        return this.http.post('/api/delete-todo', { params: params }, options)
                             .map((resp: Response) => resp.json().data);
    }
}
