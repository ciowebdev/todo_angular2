import * as mongoose from 'mongoose';
import * as mongooseShema from 'mongoose-schema-extend';
import * as Promisefrom from 'bluebird';

mongoose.Promise = Promise;
const Schema = mongoose.Schema;

const todoSchema = new Schema({
    title: {
        type: String,
        required: true,
    },
    completed: {
        type: Boolean,
        required: true,
    },
    plannedDate: {
        type: Date,
        required: false,
    }

});

const Todo = mongoose.model('todo', todoSchema);

export = Todo;

