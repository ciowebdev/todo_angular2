import * as mongoose from 'mongoose';
import * as mongooseShema from 'mongoose-schema-extend';
import * as Promisefrom from 'bluebird';

mongoose.Promise = Promise;
const Schema = mongoose.Schema;

const about_usersSchema = new Schema({
    id: {
        type: Number,
        unique: true,
        index: true
    },
    name: {
        type: String,
        required: true,
    },
    position: {
        type: String,
        required: true,
    },
    photo: {
        type: String,
        required: false,
    },

});

const AboutUser = mongoose.model('users', about_usersSchema);

export = AboutUser;
