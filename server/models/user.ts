import * as mongoose from 'mongoose';
import * as mongooseShema from 'mongoose-schema-extend';
import * as Promisefrom from 'bluebird';
import * as UserRoles from './user.roles';

mongoose.Promise = Promise;
const Schema = mongoose.Schema;

const usersSchema = new Schema({
    name: {
        type: String,
        required: true,
    },
    role: {
        type: Schema.Types.ObjectId,
        ref: 'userRoles',
        required: true,
    },
    password: {
        type: String,
        required: true,
    }
});

const User = mongoose.model('user', usersSchema);

export = User;
