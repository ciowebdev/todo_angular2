import * as mongoose from 'mongoose';
import * as mongooseShema from 'mongoose-schema-extend';
import * as Promisefrom from 'bluebird';

mongoose.Promise = Promise;
const Schema = mongoose.Schema;

const userRolesSchema = new Schema({
    name: {
        type: String,
        required: true,
    },

});

const UserRoles = mongoose.model('userRoles', userRolesSchema);

export = UserRoles;
