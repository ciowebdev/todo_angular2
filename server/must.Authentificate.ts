
export function mustAuthenticatedMw(req, res, next) {
    req.isAuthenticated()
    ? next()
    : res.json('Access denied');
};

