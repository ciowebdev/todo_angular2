import * as express from 'express';
import * as path from 'path';
import { Server as Http } from 'http';
import * as bodyParser from 'body-parser';
import * as mongoose from 'mongoose';
import * as api from './routes/api';
import * as passport from 'passport';
import { Strategy } from 'passport-local';
import * as User from './models/user';
import * as expressSession from 'express-session';
import * as about_users from './routes/about_users';

passport.use(new Strategy(
  function(username, password, cb) {

    User.findOne({ name: username }, function(err, user) {
      if (err) { return cb(err); }
      if (!user) { return cb(null, false); }
      if (user.password !== password) { return cb(null, false); }
      return cb(null, user);
    });
  }));
passport.serializeUser(function(user, cb) {
  cb(null, user.id);
});

passport.deserializeUser(function(id, cb) {
  User.findById(id, function (err, user) {
    if (err) { return cb(err); }
    cb(null, user);
  });
});

const app = express();
mongoose.connect('mongodb://localhost/todo');
mongoose.connection.on('error', function (err) {
  console.log('Mongoose default connection error: ' + err);
  process.exit(0);
});
app.use(require('cookie-parser')());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(expressSession({ secret: 'secret', resave: false, saveUninitialized: false}));
app.use(passport.initialize());
app.use(passport.session());
app.use('/', api);
app.use('/about_users/', about_users);
const port = process.env.PORT || '3000';

const httpServer: Http = app.listen(port, 'localhost', () => {
  console.log('Listening on localhost:%s', port);
});
