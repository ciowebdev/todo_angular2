
import * as express from 'express';
import * as Todo from './../models/todo';
import { mustAuthenticatedMw } from './../must.Authentificate';
import * as passport from 'passport';
const router = express.Router();

/* GET api listing. */

router.get('/', (req, res) => {
    res.send('api works');
});
router.post('/login',
  passport.authenticate('local', { failureRedirect: '/login' }),
  function(req, res) {
    res.redirect('/');
  });
router.get('/logout',
  function(req, res){
    require('express-passport-logout')();
    res.redirect('/');
  });
router.get('/api/get-todos', mustAuthenticatedMw, (req, res, next) => {
    Todo.find((err, Todos) => {
        if (err) {
            res.json({info: 'error during find Todos', error: err});
        };
        res.json({info: 'Todos found successfully', data: Todos});
    }).sort({plannedDate: 'desc'});
});
router.post('/api/add-todo', mustAuthenticatedMw, (req, res) => {
    const newTodo = new Todo();
    newTodo.title = req.body.params.title;
    newTodo.plannedDate = req.body.params.plannedDate;
    newTodo.completed = false;
    newTodo.save((err) => {
        if (err) {
            res.json({info: 'error during Todo create', error: err});
        }
        Todo.find((err1, Todos) => {
           res.json({info: 'Todo add successfully', data: Todos});
    });
    });
});

router.post('/api/delete-todo', mustAuthenticatedMw, function (req, res) {
    const query = { title: req.body.params};
    Todo.findOneAndRemove(query, function(err) {
        if (err) {
            res.json({info: 'error during find Todo', error: err});
        };
            Todo.find((err1, Todos) => { res.json({info: 'Todo delete successfully', data: Todos });
    });

    });
});

export = router;
