
import * as express from 'express';
import * as AboutUser from './../models/about_users';

const router = express.Router();

/* GET api listing. */

router.get('/get', (req, res) => {
    AboutUser.find((err, AboutUsers) => {
        if (err) {
            res.json({info: 'error during find Todos', error: err});
        };
        res.json({info: 'AboutUser found successfully', data: AboutUsers});
    }).sort({id: 'desc'});
});
router.post('/add', (req, res) => {
    const  person = req.body;
    new AboutUser({ name: person.name, position: person.position, photo: person.photo })
      .save(function (err) {
        if (err) {
          res.status(504);
          res.end(err);
        } else {
          console.log('user saved');
          res.end();
        }
      });
});

router.post('/delete', function (req, res) {
    const query = { title: req.body.params};
    AboutUser.findOneAndRemove(query, function(err) {
        if (err) {
            res.json({info: 'error during find AboutUser', error: err});
        };
            AboutUser.find((err1, AboutUsers) => { res.json({info: 'AboutUser delete successfully', data: AboutUsers });
    });

    });
});

export = router;
